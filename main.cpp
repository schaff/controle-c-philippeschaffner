#include <iostream>
#include <stdlib.h>
#include <time.h>
#include <stdbool.h>

using namespace std;

int limitationChoix () //bloque le choix du pari dans le jeu roulette
{
    int choix;

    cout<<"Faite vos jeux !"<<endl;
    cout<<"Vous choisissez paire(0) ou impaire(1) ?:"<<endl;
    cin>>choix;

    if (choix==0) //Purement d�coratif! j'affiche juste s'il a choisi paire ou impaire
    {
        cout<<"Vous avez choisi paire!"<<endl;
    }
    else
    {
        cout<<"Vous avez choisi impaire!"<<endl;
    }

    while (choix>1 || choix<0) //boucle de controle ! j'empeche l'utilisateur a choisir autre chose que 0 ou 1
    {
        cout<<"Erreur de saisie"<<endl;
        cout<<"Faite vos jeux !"<<endl;
        cout<<"Vous choisissez paire(0) ou impaire(1) ?:"<<endl;
        cin>>choix;
        cout<<"vous avez choisi : "<<choix<<endl;
    }
    return choix;
}

int controleMise (int jeton) //fonction qui empeche l'utilisateur de miser tout et n'importe quoi dans le jeu roulette
{
    int controle=0;
    int controleBis=25;
    int mise;

    cout<<"Je vous en prie misez donc : "<<endl;
    cin>>mise;

    while (mise>jeton || mise<=controle || mise>controleBis) //boucle controle de mise
    {
        if (mise<=controle) // interdir la mise de 0 et de nombre n�gatif
        {
            cout<<"Mise trop faible"<<endl;
            cout<<"Augmentez votre mise SVP !"<<endl;
        }
        else if (mise>controleBis) // interdire la mise de plus de 25
        {
            cout<<"Mise trop Importante"<<endl;
            cout<<"Baissez votre mise SVP !"<<endl;
        }
        else if (mise>jeton) // interdire la mise sup�rieur au nombre de jeton disponible
        {
            cout<<"Vous etes deja assez endette comme �a ! Inutile d'empirer votre cas !"<<endl;
            cout<<"Baissez votre mise SVP !"<<endl;
        }

        cout<<"Je vous en prie remisez donc : "<<endl;
        cin>>mise;
    }
    return mise;
}

int testroulette (int jeton, int mise, int lancerJoueur, int lancerOrdi) // test le jeu de la roulette
{
    lancerOrdi=rand()%37; //lancer de la bille dans la roulette! num�ro random.

    if (lancerOrdi == 0)// if z�ro perte automatique
    {
        cout<<"Dommage vous perdez votre mise!"<<endl;
        cout<<"Le resultat est de : "<<lancerOrdi<<endl;
        jeton=jeton-mise;
    }
    else if (lancerJoueur == lancerOrdi%2) // if gagnant
    {
        cout<<"Bravo ! C'est bien tu as gagne ! Mais pour combien de temps ?"<<endl;
        cout<<"Le resultat est de : "<<lancerOrdi<<endl;
        jeton=jeton+mise;
    }
    else // if perdant
    {
        cout<<"Dommage vous perdez votre mise!"<<endl;
        cout<<"Le resultat est de : "<<lancerOrdi<<endl;
        jeton=jeton-mise;
    }
    cout<<"Il vous reste "<<jeton<<" jeton"<<endl<<endl;
    return jeton;
}

int roulette(int jeton) //fonction du jeu roulette
{
    int mise;
    int lancerJoueur;
    int lancerOrdi;

    cout<<"Voici le jeu de la roulette, prenez place!"<<endl<<endl;

    while (jeton>=1 && jeton<100)
    {
        mise=controleMise(jeton); //appel de la fonction controle de la mise
        lancerJoueur=limitationChoix(); // appel de la fonction limitation du choix du pari (paire ou impaire)
        jeton=testroulette(jeton, mise, lancerJoueur, lancerOrdi); // appel de la fonction test
    }
    return jeton;
}

int rouletteRusse (int jeton) //fonction du jeu roulette russe
{
    int bullet=rand()%6;
    int tab[6];
    int retourRoulette;
    bool chance=false; // un bool qui me permet de sortir de mon while et retourner dans le while du main.

    for (int i=0; i<=5; ++i) // initialisation du tableau de 6 cases a 0
    {
        tab[i]=0;
    }

    tab[bullet]=1;

    cout<<"Comme tu n'as plus de jeton, tu vas miser ta vie ! Que la chance soit avec toi !"<<endl<<endl;

    while (chance==false) //boucle de la roulette russe
    {
        if (tab[0]==1)//if perdu
        {
            cout<<"Oh ! C'est dommage tu es mort ! Mais tu as toujours une dette. Nous allons voir si ta femme est plus chanceuse !"<<endl;
            chance=true;
            jeton=0;
        }
        else if (tab[0]==0)//if gagnant
        {
            cout<<"CLICK!"<<endl;
            cout<<"Je vois que tu es un peu plus chanceux quand ta vie est en jeu ! Ca ne durera pas."<<endl;
            jeton=jeton+20;//ajout de 20 jetons
            cout<<"Tu as nouveau "<<jeton<<" jetons! Tu veux retourner a la roulette ? (Oui(0)/Non(1))"<<endl;
            cin>>retourRoulette;
            if (retourRoulette==0) //permet de laisse le choix au joueur de continuer ou non la roulette russe ou de basculer a nouveau a la roulette
            {
                chance=true;
            }
            else
            {
                chance=false;
                tab[bullet]=0;
                --bullet;//rapproche la balle du percuteur
                tab[bullet]=1;
            }

        }
    }
    return jeton;



}

int main()
{
    int jeton=10;
    bool estMort=false;
    srand (time(NULL));

    cout<<"Bonjour et bienvenue au Casino! Bien dormi ?"<<endl;
    cout<<"Veuillez regler vos dettes ou mourrez !"<<endl<<endl;

    while (jeton<100 && estMort==false)//boucle principale du code
    {
        if (jeton>0) //perme de rentrer dans la boucle de la roulette
        {
            jeton=roulette(jeton);//mise a jour des jetons
        }
        else
        {
           jeton=rouletteRusse(jeton);//mise a jour des jetons
           if (jeton==0)//permet de rentrer dans la boucle de la roulette russe
           {
               estMort=true;//permet de sortir de la boucle si le joueur a perdu � la roulette russe.
           }
        }
    }

    if (jeton>=100) //d�termine l'�criture � la fin du code en fonction du r�sultat.
    {
        cout<<"Bravo tu peux partir ! Ne reviens jamais ici !"<<endl;
    }
    else
    {
        cout<<"GAME OVER!!!"<<endl;
    }

    return 0;
}
